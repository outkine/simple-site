% Phantasy Star Online: Blue Burst on Wine
% /dev/urandom
% 2020-08-02

Occasionally I play the old multiplayer action-RPG "Phantasy Star
Online: Blue Burst". On Windows, it works fine, but to make it work better on
Linux (via Lutris), some extra steps may be needed.

<!-- cut -->

> %info%
>
> Данный пост также доступен [на русском языке](psobb_wine_ru.html).
>

**First of all, install the game** using the ["EphineaPSO"
installer](https://lutris.net/games/phantasy-star-online-blue-burst/). This
installer uses a version of the game designed for the
[Ephinea](https://ephinea.pioneer2.net/) private server, which, to my knowledge,
is the most popular one at the moment.

After this, the game should be perfectly playable, but on my machine, it had one
minor graphical error:

## missing scrolling textures

the transparent scrolling textures that the game often uses to display computer
monitors were invisible (see screenshots
[here](https://twitter.com/_dev_urandom_/status/1289888125334859778)).

I tried using different versions of Wine, different settings in Lutris, and more
to no avail, until I discovered [this
thread](https://www.pioneer2.net/community/threads/make-psobb-pretty-and-run-smoothly.18372/)
on the Ephinea forums. It describes a utility called "dgVoodoo" that can rewire an
old Direct3D 8/9 program to use the APIs of DirectX 11. Given that, in current
versions of Wine, most effort is being spent on DirectX 11, trying it out seemed
like a good idea.

The official website mentioned in the forum post isn't active, but the program
is still being actively developed [on
GitHub](https://github.com/dege-diosg/dgVoodoo2/releases).

However, when I first installed it, it resulted in an even worse outcome -- the
game launched with a permanent black screen instead of the graphics. Luckily
enough, it turned out that the solution to _that_ was even simpler: to turn on
the "Enable DXVK/VKD3D" option in Lutris' settings for the game. After this, the
game ran perfectly, and the previously-missing textures returned (see [this
excited tweet](https://twitter.com/_dev_urandom_/status/1289943626827460613) for
a screenshot with confirmation).

So, **to fix the scrolling textures**:

 * download the latest release of dgVoodoo2
   [from GitHub](https://github.com/dege-diosg/dgVoodoo2/releases). 

 * Extract the libraries from the `MS/x86` directory into the game's executable
   directory (in Lutris, right-click PSO's menu item, use the "Browse files"
   menu item, then open "drive\_c" and "EphineaPSO" folders.)

 * Open winecfg (in Lutris, click the Wine icon at the bottom of the window,
   then choose "Wine configuration").

 * In winecfg, open the "Libraries" tab, type in "d3d8" into the dropdown menu
   and click "Add". Then do the same for "d3d9" and "d3dimm". This will make
   sure this installation will use dgVoodoo2's libraries.

 * In Lutris, open the game configuration window (right-click on the PSO menu
   item, then choose "Configure"), take a look at the "Runner options" tab and
   make sure the "Enable DXVK/VKD3D" switch is **on**.

 * Run the game. It should display the "dgVoodoo" watermark in the bottom right.
   To make sure the scrolling textures work, log in, and then transport your
   character into lobby 11. There should be a bunch of transparent displays
   showing scrolling unreadable text.

And **to get rid of the watermark**:

 * Extract the "dgVoodooCpl.exe" file from the dgVoodoo2 archive somewhere.

 * In Lutris, run this file inside PSO's wine prefic (click the Wine icon at the
   bottom of the window, then choose "Run EXE inside Wine prefix" and select the
   `dgVoodooCpl.exe` file.

 * In the window that opens, open the "DirectX" tab, uncheck the "dgVoodoo
   Watermark" option and click "OK".

 * Run the game. It should still run using dgVoodoo, but without the watermark.

## russian text display and input

If you speak Russian and want to talk to, or read text from, other Russian
speakers, you might need to change fonts and some settings.

 * First of all, **let's set up Russian input**. In Lutris, open the game's
   configuration window, the "System options" tab, the "Environment variables"
   list, click "Add" and in the list item that appears, change the "Key" to
   "`LANG`" and "Value" to "`ru_RU.UTF-8`". This setting will make sure Wine
   pretends to be a Russian version of Windows and uses the appropriate locale
   settings when possible.

 * If this works, then when you type Russian text in chat, it will be displayed
   as incorrect Latin characters (if this doesn't, it will be rendered as
   question marks, which is worse). Now, let's set up the font.

 * The "ArPSO" font is a modified version of the "Arial" font for use in
   Phantasy Star Online: Blue Burst. All Windows-1252 letters have been manually
   remapped to their Windows-1251 equivalents. Visit [this
   link](https://nextstage.ru/threads/phantasy-star-online-blue-burst.7149/) and
   then the link labeled "arpso.zip".

 * Extract the file "arpso.ttf" into the "`C:\Windows\Fonts`" directory of PSO's
   wine prefix (right-click on the PSO menu item, then choose "Browse files",
   navigate to "`drive_c`", then "`Windows`", then "`Fonts`".

 * In the PSO launcher, choose "Options", then click the "More" button. In the
   window that appears, use the "Font" dropdown menu to choose "ArPSO". Then
   click "Save".

 * Run the game. The font would change into one that allows to see Russian
   characters.

### alternative option: LibPSO font

Alternatively, I've made another font, called "LibPSO", which does the same
thing as "ArPSO", but is based on "Liberation Sans Bold" instead of "Arial".
In my opinion, this font looks better than "ArPSO" when used in Wine.

 * [Download font](/data/LibPSO.zip)

The installation procedure is the same: extract the TTF file into the wine
prefix's `C:\Windows\Fonts` directory and choose "LibPSO" in the game's
settings.

---

