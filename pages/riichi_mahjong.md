% riichi mahjong
% /dev/urandom
% march 2021

One of my big interests is things relating to *riichi mahjong* (リーチ麻雀): a
variant of the Chinese tabletop game "mahjong" invented in Japan. It's a rather
complicated, but fun game where the players take turns picking and discarding
tiles (which function similar to cards in a game like poker) in order to build a
winning hand.

Riichi mahjong is extremely popular in Japan, and its popularity there also led
to its appearance in a lot of Japanese works made for international markets:
video games such as the *Yakuza* series and *Final Fantasy XIV* include mahjong
minigames, and board game collections like Nintendo's *Clubhouse Games: 51
Worldwide Classics* feature riichi mahjong as part of its international
collection of board and party games.

In addition, the game is subject of several manga and anime series, which has
also spread interest in it among fans of Japanese pop-culture. *Saki*, *Akagi*
and *The Legend of Koizumi* are the most well-known among many series based
around the game.

# learn the game

* [Exhaustive wiki on arcturus.su](http://arcturus.su/wiki/Main_Page)

* [Educational videos by Light
  Grunty](https://www.youtube.com/c/LightGrunty/videos)

# play the game

* [MahjongSoul, a newbie-friengly English-language online game with some gacha
  mechanics](mahjongsoul.game.yo-star.com/)

* [Tenhou, a more traditional free online game more suited for competitive play.
  Supports Japanese, English and other languages, but the translations are
  limited](https://tenhou.net/3/)

* * Browser add-on that implements complete English and French translations for
    the Tenhou web app: [GitLab
    Source](https://gitlab.com/zefiris/tenhou-english-ui) /
    [Firefox](https://addons.mozilla.org/en-US/firefox/addon/tenhou-english/) /
    [Chrome](https://chrome.google.com/webstore/detail/tenhou-english-ui/cbomnmkpjmleifejmnjhfnfnpiileiin)

* [Kemono Mahjong, an Android game available in multiple languages, including
  English, Russian and
  Japanese](https://play.google.com/store/apps/details?id=com.kemonomahjong.app)

# irl

Back in late 2019 and early 2020, before the COVID-19 pandemic has made
gatherings for purpose of entertainment a risky idea, I used to play mahjong in
a club called "[Tesuji](http://tesuji-club.ru/)". Right now the club doesn't do
game nights (probably because of the aforementioned pandemic), but their [VK
group](https://vk.com/tesuji) is still very active, with puzzles, translations
of different works and such.

