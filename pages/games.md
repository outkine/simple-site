% rnd's games
% /dev/urandom
% april 2020

# Donsol for PICO-8

![Screenshot](https://www.lexaloffle.com/media/44150/donsol.p8_020.png)

[See rules, play or download on PICO-8 BBS](https://www.lexaloffle.com/bbs/?tid=37711)

This is a port of the game "Donsol", originally designed by [John
Eternal](https://twitter.com/johneternal) and implemented first as an Electron
app, then [as a Famicom/NES game](https://hundredrabbits.itch.io/donsol) by
Devine Lu Linvega and Rekka Bellum of ["Hundred
Rabbits"](https://100r.co/site/home.html).

The game is a dungeon-crawler designed around a standard deck of 54 cards.

[TIC-80 version, has almost all of the features](https://tic80.com/play?cart=1586)

