% словарь toki pona
% /dev/urandom
% июль 2020

Эта страница содержат список всех официальный слов toki pona, описанных на
страницах с 1 по 12 в алфавитном порядке. Можно щёлкнуть на слово и перейти на
урок, в котором оно впервые упоминается. У каждого слова упомянуты несколько его
возможных значений, зависящих от того, какой частью речи оно является в
предложении.

Определение в поле "прилагательное" также применяется к причастиям, поскольку в
языке toki pona разница между ними заключается лишь в том, применяются ли они к
существительным или глаголам.

Существительное всегда можно использовать как прилагательное в значении
родительного падежа.  Например, "moku soweli" может значить "животная еда" (либо
"еда для животных", либо "мясо", в зависимости от контекста).

> %note% Этот словарь не является копией словаря из официальной книги или любого
> другого курса. Некоторые слова могут применяться как части речи, не указанные
> здесь. Также здесь упомянутые некоторые необычные значения (необычное зн.)
> слов, которые зачастую применяются только в комбинации с другими словами.
> 
> В языке toki pona нет заранее прописанных фраз или значений слов, и вы можете
> придумывать свои собственные значения, зависящие от контекста. Самое важное
> правило -- это чтобы вас поняли ваши собеседники.

---

#### [a](ru_7.html)
* частица: (эмоциональное междометие, подтверждение)

#### [akesi](ru_12.html)
* существительное: "некрасивое" животное, ящерица

#### [ala](ru_2.html)
* существительное: ничего
* прилагательное: нет, не-, пустой
* число: ноль

#### [alasa](ru_12.html)
* глагол: охотиться, собирать (ресурсы)
* глагол (необычное зн.): искать

#### [ale/ali](ru_2.html)
* существительное: изобилие, всё, вселенная
* прилагательное: каждый, все, в изобилии
* число: всё/бесконечность (простая система), 100 (сложная система)

#### [anpa](ru_7.html)
* существительное (старое зн.): нижняя часть
* прилагательное: преклоняющийся, нижний, ничтожный, скромный, зависимый
* глагол w/o obj: преклоняться
* глагол w/ obj: завоёвывать, побеждать, преклонять (других)

#### [ante](ru_5.html)
* существительное: разница, изменение, (необычное зн.) версия
* прилагательное: другой, изменённый
* глагол: менять

#### [anu](ru_7.html)
* частица: или

#### [awen](ru_5.html)
* существительное (необычное зн.): стабильность, безопасность, ожидание
* прилагательное: хранимый, безопасный, стойкий, ждущий, остающийся
* глагол: хранить, оставаться, защищать, беречь
* вспом. глагол: продолжать (что-то делать)

#### [e](ru_3.html)
* частица: (указывает дополнение/объект глагола)

#### [en](ru_5.html)
* частица: и (совмещает подлежащие)

#### [esun](ru_11.html)
* существительное: торговля, обмен, транзакция
* прилагательное: торговый, обменный, коммерческий
* глагол: продавать/покупать, торговать, меняться

#### [ijo](ru_3.html)
* существительное: вещь, объект, материя
* прилагательное: материальный, физический

#### [ike](ru_1.html)
* существительное: зло
* прилагательное: плохой, злой, сложный, ненужный
* глагол (необычное зн.): делать что-то хуже

#### [ilo](ru_3.html)
* существительное: инструмент, машина, устройство
* прилагательное: полезный, (необычное зн.) электронный, металлический

#### [insa](ru_7.html)
* существительное: внутренности, содержимое, центр, живот
* прилагательное: центральный, внутренний, серединный

#### [jaki](ru_8.html)
* существительное: грязь, мусор, отходы
* прилагательное: грязный, отвратительный, ядовитый, нечистый, несанитарный
* глагол: загрязнять, отравлять

#### [jan](ru_2.html)
* существительное: человек, человечество, люди, кто-то
* прилагательное: человеческий, личный, народный

#### [jelo](ru_8.html)
* существительное/прилагательное: желтый (и его оттенки)
* глагол: красить нечто желтым цветом

#### [jo](ru_4.html)
* существительное: (необычное зн.) владения, собственность
* глагол: держать, иметь, содержать, нести

#### [kala](ru_4.html)
* существительное: рыба, морское существо

#### [kalama](ru_5.html)
* существительное: звук, шум
* прилагательное: громкий, шумный, звучащий, звуковой
* глагол: шуметь, читать вслух, играть (на муз. инструменте)

#### [kama](ru_10.html)
* существительное: событие, прибытие
* прилагательное: приходящий, идущий, будущий, призванный
* вспом. глагол: стать, становиться, успешно что-то сделать

#### [kasi](ru_4.html)
* существительное: растения, травы, листья

#### [ken](ru_10.html)
* существительное: возможность, способность, (необычное зн.) право, свобода
* прилагательное (необычное зн.): возможный, способный
* вспом. глагол: может (что-то сделать)

#### [kepeken](ru_6.html)
* существительное: применение, (необычное зн.) практика
* глагол w/ obj: использовать что-то
* предлог: (сделать что-то) с помощью, используя

#### [kili](ru_1.html)
* существительное: фрукты, овощи, грибы

#### [kiwen](ru_4.html)
* существительное: твёрдый объект, металл, камень
* прилагательное: твёрдый, из твёрдого материала, металлический

#### [ko](ru_4.html)
* существительное: порошок, глина, паста, нечто полутвёрдое
* прилагательное: порошковый, глиняный, полутвёрдый

#### [kon](ru_12.html)
* существительное: воздух, сущность, дух, (необычное зн.) газ
* прилагательное: невидимый, бесплотный, (необычное зн.) газообразный

#### [kule](ru_8.html)
* существительное: цвет, (редкое зн.) пол/гендер (человека)
* прилагательное: цветной, крашеный
* глагол: красить что-то

#### [kulupu](ru_5.html)
* существительное: группа, сообщество, компания, общество, нация, племя
* прилагательное: общественный, социальный

#### [kute](ru_7.html)
* существительное: ухо, слух
* прилагательное: ...-звучащий
* глагол: слушать, слышать, подчиняться

#### [la](ru_9.html)
* частица: "если/когда" (вводит контекст)

#### [lape](ru_5.html)
* существительное: сон, отдых
* прилагательное: спящий, отдыхающий, для сна/отдыха
* глагол: спать, отдыхать

#### [laso](ru_8.html)
* существительное/прилагательное: синий, голубой, зелёный (и их оттенки)
* глагол: красить нечто синим/зелёным цветом

#### [lawa](ru_7.html)
* существительное: голова, ум, мозг
* прилагательное: основной, главный, управляющий, правящий
* глагол: управлять, контролировать, направлять, править, владеть

#### [len](ru_9.html)
* существительное: одежда, ткань, нечто защищающее частную жизнь
* прилагательное: одетый, сделанный из ткани
* глагол: одевать, защищать частную жизнь

#### [lete](ru_9.html)
* существительное: холод, мороз
* прилагательное: холодный, прохладный, сырой
* глагол: охлаждать

#### [li](ru_1.html)
* частица: (между подлежащим и глаголом/прилагательным)

#### [lili](ru_1.html)
* существительное: маленький размер
* прилагательное: маленький, небольшой, в небольшом количестве, молодой, ничтожный
* глагол: уменьшать

#### [linja](ru_9.html)
* существительное: длинный гибкий объект, верёвка, резинка, лента, волосы

#### [lipu](ru_3.html)
* существительное: плоский объект, книга, документ, бумага, страница, запись, вебсайт
* прилагательное: плоский, используемый для записи

#### [loje](ru_8.html)
* существительное/прилагательное: красный (и его оттенки)
* глагол: красить нечто красным цветом

#### [lon](ru_6.html)
* существительное: истина, правда жизнь, существование
* прилагательное: истинный, правдивый, существующий
* глагол w/o object: существует
* предлог: в, на (местоположение/время)

#### [luka](ru_9.html)
* существительное: рука
* прилагательное: ручной
* число: 5 (сложная система)

#### [lukin](ru_3.html)
* существительное: глаз, зрение
* прилагательное: ...-выглядящий, визуальный
* глагол: смотреть, видеть, читать
* вспом. глагол: пытаться, стремиться (что-то сделать)

#### [lupa](ru_10.html)
* существительное: дыра, дверь, окно

#### [ma](ru_4.html)
* существительное: земля, территория, страна, открытый воздух

#### [mama](ru_2.html)
* существительное: родитель, предок, создатель, источник, опекун
* глагол: создавать, служить родителем, опекать

#### [mani](ru_11.html)
* существительное: деньги, крупные одомашненные животные
* прилагательное: (необычное зн.) достаточный

#### [meli](ru_2.html)
* существительное: женщина, самка, жена
* прилагательное: женственная

#### [mi](ru_1.html)
* существительное: я, мы, они
* прилагательное: мой, наш

#### [mije](ru_2.html)
* существительное: мужчина, самец, муж
* прилагательное: мужественный

#### [moku](ru_2.html)
* существительное: еда
* прилагательное: съедобный
* глагол: есть, пить, глотать

#### [moli](ru_8.html)
* существительное: смерть
* прилагательное: мёртвый, умирающий
* глагол: убивать

#### [monsi](ru_6.html)
* существительное: спина, задняя часть, задница
* прилагательное: задний

#### [mu](ru_7.html)
* (звук животного)

#### [mun](ru_11.html)
* существительное: луна, звезда, объект ночного неба
* прилагательное: лунный, звёздный

#### [musi](ru_9.html)
* существительное: игра, искусство
* прилагательное: интересный, развлекательный, художественный
* глагол: играть, веселить, развлекаться

#### [mute](ru_5.html)
* существительное: количество
* прилагательное: много, больше
* число: 3 или больше (простая система), 20 (сложная система)

#### [nanpa](ru_11.html)
* существительное: число
* прилагательное: -ый (порядковый номер), математический, числовой,
    (необычное зн.) цифровой

#### [nasa](ru_8.html)
* прилагательное: странный, необычный, пьяный

#### [nasin](ru_10.html)
* существительное: путь, дорога, улица, указание, уклад, обычай, принцип
* прилагательное: дорожный, уличный, следующий укладу/обычаю/принципу
* глагол (необычное зн.): указывать, показывать дорогу

#### [nena](ru_10.html)
* существительное: холм, гора, кнопка, неровность, нос

#### [ni](ru_5.html)
* существительное/прилагательное: этот, тот

#### [nimi](ru_7.html)
* существительное: слово, имя

#### [noka](ru_6.html)
* существительное: нога, низ, нижняя часть, под (чем-то)

#### [o](ru_7.html)
* частица: (обращение к кому-либо, указание)

#### [olin](ru_3.html)
* существительное: любовь, сострадание, привязанность, уважение
* прилагательное: любимый, уважаемый
* глагол: любить, сострадать, уважать

#### [ona](ru_1.html)
* существительное: он, она, оно, они
* прилагательное: его, её, их

#### [open](ru_10.html)
* существительное: начало
* прилагательное: начальный
* глагол: начинать, открывать, включать
* вспом. глагол: начинать (что-то делать)

#### [pakala](ru_5.html)
* существительное: вред, повреждение, ошибка
* прилагательное: поломанный, неправильный
* глагол: ломать, делать ошибки
* частица: (ругательство)

#### [pali](ru_3.html)
* существительное: работа, труд
* прилагательное: рабочий
* глагол: работать, делать

#### [palisa](ru_9.html)
* существительное: длинный твёрдый объект, ветвь, палка, (необычное зн.) длина
* прилагательное: длинный

#### [pan](ru_12.html)
* существительное: хлеб, зерно, кукуруза, рис, пицца

#### [pana](ru_3.html)
* прилагательное: (необычное зн.) данный, отправленный, выпущенный
* глагол: давать, отправлять, излучать, выпускать

#### [pi](ru_9.html)
* частица: (группирует прилагательные/причастия)

#### [pilin](ru_12.html)
* существительное: сердце, чувство, прикасания
* прилагательное: чувственный, эмоциональный, касательный
* глагол: трогать, думать, чувствовать

#### [pimeja](ru_8.html)
* существительное: чёрный (и его оттенки), тень
* прилагательное: чёрный, тёмный
* глагол: красить нечто чёрным/тёмным цветом, затенять

#### [pini](ru_10.html)
* существительное: конец, финал, финиш
* прилагательное: финальный, завершённый, прошлый (с tenpo)
* глагол: заканчивать, закрывать, выключать
* вспом. глагол: заканчивать что-то делать

#### [pipi](ru_4.html)
* существительное: насекомое, жук

#### [poka](ru_6.html)
* существительное: бок, сторона, близлежащее место
* прилагательное: соседствующий, недалёкий, находящийся с чьей-то стороны

#### [poki](ru_11.html)
* существительное: коробка, ёмкость, тарелка, чаша, ящик, контейнер
* глагол (необычное зн.): класть в poki

#### [pona](ru_1.html)
* существительное: добро, простота
* прилагательное: хороший, добрый, простой, дружественный, мирный
* глагол: улучшать, исправлять

#### [pu](ru_12.html)
* существительное: официальная книга о языке toki pona
* прилагательное: соответствующий официальной книге
* глагол: взаимодействовать с официальной книгой

> %note%
> В самой официальной книге слово "pu" имеет значение только как глагол. (Хотя
> фраза "pu la" используется в ней в значении "в этой книге".)
> Некоторые в сообществе toki pona используют это слово только как глагол, а
> некоторые -- и как другие части речи.

#### [sama](ru_6.html)
* существительное: схожесть, брат/сестра
* прилагательное: похожий, такой же, братский
* предлог: такой как, похож на

#### [seli](ru_5.html)
* существительное: тепло, жар, химическая реакция, источник тепла
* прилагательное: тёплый, горячий
* глагол: обогревать

#### [selo](ru_9.html)
* существительное: внешняя форма, оболочка, кожа, граница
* прилагательное: внешний

#### [seme](ru_7.html)
* частица: что? какой? (для вопросов)

#### [sewi](ru_6.html)
* существительное: высокое место, возвышенность, вершина, небо, бог
* прилагательное: высокий, выше, божественный, священный

#### [sijelo](ru_9.html)
* существительное: тело, физическое состояние, торс

#### [sike](ru_11.html)
* существительное: круг, шар, цикл, колесо, (с tenpo) год
* прилагательное: круглый, сферический, годовой
* глагол: окружать

#### [sin](ru_11.html)
* существительное: новинка, дополнение, (необычное зн.) обновление, приправа
* прилагательное: новый, дополнительный, свежий
* глагол: добавлять, обновлять

#### [sina](ru_1.html)
* существительное: ты/вы
* прилагательное: твой/ваш

#### [sinpin](ru_6.html)
* существительное: лицо, передняя часть, перед, стена

#### [sitelen](ru_4.html)
* существительное: символ, изображение, письмо
* прилагательное: символический, написанный, записанный, нарисованный
* глагол: писать, рисовать, записывать

#### [sona](ru_10.html)
* существительное: знание, информация
* прилагательное: известный
* глагол: знать, уметь
* вспом. глагол: уметь (что-то сделать)

#### [soweli](ru_1.html)
* существительное: животное, наземное млекопитающее, зверь

#### [suli](ru_1.html)
* существительное: размер, величие
* прилагательное: большой, тяжёлый, высокий, великий, важный, взрослый
* глагол: расти

#### [suno](ru_11.html)
* существительное: солнце, свет, яркость, источник света
* прилагательное: солнечный, яркий
* глагол: светить, освещать, сиять

#### [supa](ru_12.html)
* существительное: горизонтальная поверхность

#### [suwi](ru_2.html)
* существительное: (необычное зн.) сладости, ароматы
* прилагательное: сладкий, хорошо пахнущий, милый, прелестный

#### [tan](ru_6.html)
* существительное: причина, происхождение
* прилагательное: оригинальный
* глагол w/ object (необычное зн.): служить причиной (чему-либо)
* предлог: из, из-за, потому что

#### [taso](ru_12.html)
* частица (в начале предложения): но, однако
* прилагательное: только

#### [tawa](ru_6.html)
* существительное: движение
* прилагательное: движущийся
* глагол: двигать, двигаться
* предлог: в/на (направление), для, с точки зрения

#### [telo](ru_3.html)
* существительное/прилагательное: вода, жидкость
* прилагательное: сырой, жидкий, водяной
* глагол: мыть, поливать

#### [tenpo](ru_10.html)
* существительное: время, момент, случай
* прилагательное: временной

#### [toki](ru_4.html)
* существительное: речь, разговор, язык
* прилагательное: разговорный
* глагол: говорить, разговаривать, использовать язык, думать

#### [tomo](ru_3.html)
* существительное: дом, здание, структура, интерьер, комната
* прилагательное: домашний, в закрытых помещениях

#### [tonsi](ru_x1.html)
* существительное: небинарный человек, транс-человек
* прилагательное: гендерно-неконформный, транс-

> %warning%
> "tonsi" -- единственное неофициальное слово в этом списке. Это слово было
> придумано сообществом после выхода официальной книги и является самым
> воспринятым новым словом среди сообщества.

#### [tu](ru_11.html)
* число: 2
* существительное: разделение
* прилагательное: разделённый
* глагол: делить

> %warning%
> Слово "tu" в конце фразы обычно обозначает число 2. Чтобы выразить смысл "разделённый",
> нужно использовать частицу "li":
>
> kulupu tu -- два сообщества
>
> kulupu li tu. -- сообщество разделено.

#### [unpa](ru_8.html)
* существительное: секс
* прилагательное: сексуальный
* глагол: заниматься сексом

#### [uta](ru_12.html)
* существительное: рот, губы, уста

#### [utala](ru_2.html)
* существительное: битва, драка, соревнование, вызов, война
* прилагательное: агрессивный, военный
* глагол: сражаться, воевать, соревноваться

#### [walo](ru_8.html)
* существительное: белый (и его оттенки)
* прилагательное: белый, яркий, светлый
* глагол: красить нечто белым цветом

#### [wan](ru_11.html)
* число: 1
* существительное: часть, раздел (чего-либо)
* прилагательное: объединённый, женатый
* глагол: объединять, женить

> %warning%
> Слово "wan" в конце фразы обычно обозначает число 1. Чтобы выразить смысл "объединённый",
> нужно использовать частицу "li":
>
> kulupu wan -- одно сообщество
>
> kulupu li wan. -- сообщество объединено.
>
> kulupu mute wan -- 21 сообщество (сложная система чисел)
> 
> kulupu mute li wan -- много (или 20) сообществ объединены.

#### [waso](ru_4.html)
* существительное: птица, летающее существо

#### [wawa](ru_2.html)
* существительное: сила, мощь, энергия
* прилагательное: сильный, мощный, энергичный

#### [weka](ru_12.html)
* существительное: отсутствие, отдалённость, далёкие края
* прилагательное: отсутствующий, ушедший, далёкий
* глагол: убрать, избавиться от

#### [wile](ru_10.html)
* существительное: желание, нужда
* прилагательное: желаемый, нужный, требуемый
* глагол: хотеть что-то
* вспом. глагол: хотеть делать что-то

---

[Главная страница](ru_index.html)

