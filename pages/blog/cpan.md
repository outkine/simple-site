% cpan
% /dev/urandom
% 2021-03-23

Note to self: if your distro's repositories don't have a Perl package, it's best
to install them from CPAN instead. A tool called `cpan minus` (In Alpine, its
packaged as `perl-app-cpanminus`) can do it easily, either installing packages
into one's home directory or globally.
