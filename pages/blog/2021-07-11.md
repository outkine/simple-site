% trip to vladimir and suzdal
% /dev/urandom
% 2021-07-11

So a few days ago my parents and I took a visit to one of the old friends of my
mom. During this trip, we visited the nearby cities of Suzdal' and Vladimir,
took a bunch of photos, which I want to post, but thinking about what would be
the simplest, but also most usable self-hosted option.

> %warning%
> Honestly, I still don't think visiting tourist destinations during a pandemic
> is a good idea, but I don't think arguing about it with my parents would have
> been useful. At least I am fully vaccinated and they had COVID-19 a few months
> ago, with the last test still showing significant antibody counts. We did all
> wear masks and try to follow social distancing norms, though.

> %info%
> Somewhere in 2015, said friend needed help with the computer, so I installed
> Ubuntu on it. It worked out pretty well -- at least well enough that the same
> laptop, with the same version of Ubuntu, was still there when we arrived. (It
> didn't get any system updates, and this meant that a lot of the websites were
> complaining about the web browser version, though.) I installed a newer LTS
> version of Ubuntu on that machine (but also kept the old one and all the data
> in place).

The first place was Suzdal', a town of around 10 thousand people which had a lot
of historical importance in early Russian history, but got sidelined in the 19th
and 20th century. Eventually, it was put under federal protection and became a
tourism-based town.

It started with a visit to the "museum of wooden architecture and peasant life"
(<geo:56.41300,40.43967?z=18>), a collection of wooden 18-19c buildings moved
from other cities and turned into exhibits on Russian peasants' life.

The next stop, and very close by, was the Suzdal' Kremlin
(<geo:56.41600,40.44212?z=18>), which included a museum, a couple churches and a
palace building.

At the moment, its museum featured a [temporary
exhibition](https://vladmuseum.ru/ru/exhibition/dva-kolesa/) that showcased a
selection of motorcycles formerly made at the ZiD plant, a factory located
nearby that produced (and still produces) firearms, anti-tank and anti-aircraft
guns and motorcycles. It claimed some of the motorcycles were used in
professional races and a sport called "Motoball", a variant of association
football played with everyone riding motorcycles. Judging by how short the
English-language Wikipedia entry on Motoball is compared to the Russian-language
entry, as well as the list of countries winning the European championships of
this sport, seems like it only caught on in a few Western European countries and
the former Soviet Union.

The palace building also had a more regular historical exhibition featuring
daily-use items and art from the area used from the 11th century all the way to
the early 20th century. From simple tools and "fashion accessories" to swords
and maces to icons and documents of the time. I found particularly interesting a
law book issued during Catherine the Great's reign that was written in Russian,
Latin, German (complete with fraktur font) and French at the same time.

> %info%
> As much as Peter I is disliked by Russia's conservatives, gotta give him an
> enormous thanks for standardizing the Russian alphabet and coming up with a
> single font for official documents.

After visiting the exhibitions, we decided to find a restaurant, but in a place
where the entire economy is based around tourism and protected from development,
basically everything was overpriced. When I searched my navigator app for
`Столовая №1` (a common "brand" name for affordable cafeterias in Russia), I
found a place several kilometers away in a nearby village. At least getting to
it in a car was relatively easy and it had adequate and affordable food -- about
the same quality as the cafeteria at my work.

Next day, we visited Vladimir. The first stop was the Cathedral of Saint
Demetrius (<geo:56.12921,40.41027?z=18>) -- a 12th century church featuring some
interesting reliefs on the outside and a Byzantine-style fresca depicting the
"Judgment Day" on the inside. After that, we walked around the nearby parks and
spotted a tree with ribbons and Easter Eggs tied to it. The sign posted to it
said that the tree was decorated by the citizens on the initiative of the local
women's basketball team. (Sadly, the sign was made of a golden-colored material
and had text printed on it in a way that made it really hard to make a readable
photograph.)

> %info%
> I tried looking up news articles about the egg-decorated tree, and none of
> them had the photo of the sign. I guess even professional photographers had
> problems making it look readable.

After that, we took to Bolshaya Moskovskaya street looking for interesting
places. One of them was a store purportedly selling Soviet candy, but in reality
a lot of its products were not only not Soviet (which makes sense), but not even
styled after actual candy products sold in the Soviet Union. After this, we
visited the "Baba Yaga fairytale museum" (<geo:56.12765,40.40216?z=19>). It had
a 20-minute exhibition where the guide talked about Baba Yaga and other figures
of pre-Christian Slavic mythology (that turned into folklore characters), showed
a collection of Baba Yaga dolls, including those made by children and similar
characters from other countries, and ending with an exit that led to the same
"Soviet" candy store.

Then we took a walk along Georgievskaya street, which also featured a bunch of
monuments and tourist attractions, including the Church of Saint George, the
"Old Pharmacy" building and monuments to firefighters and customs controls (and
also a water tap, which is important when it's +30°C outside with not a single
cloud in the skies!). This time, we decided not to look too hard for a
restaurant and went to a common American fast food chain (while following all
the social distancing rules and trying to minimize contact with the virus).

The next stop was the "Retro Warehouse" next to the Vladimir railway terminal
(<geo:56.12910,40.41822?z=18>). As the name says, it's a warehouse that features
an exhibition of different Soviet (and also 90s Russia) items, from photographs
and posters to televisions and typewriters to sports "wimpels" (small banners
typically of triangular shape) and other common items. There was even a small
kiosk from the days of pre-Soviet Russian Empire, judging by the sign it had.
Apparently, a lot of the items there are available for purchase, but we decided
to not buy anything and leave things as is.

This practically marked the end of the trip, as the rest of the day was occupied
with personal matters involving the aforementioned mom's friend.

Next day, we woke up unusually early and drove back to Moscow.
