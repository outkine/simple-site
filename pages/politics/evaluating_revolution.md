% evaluating a revolution's death toll?
% /dev/urandom
% 2021-06-28

> %warning%
>
> CW: death
>
> This article quite frequently talks about people dying, and in its attempt to
> come up with a way to figure out whether a (presumably socialist) revolution
> would succeed at its goals of preventing unnecessary deaths, tries to count
> deaths as points against either the government or the revolutionary forces. I
> admit that the way I talk about it is a bit callous.
>

Seems like in this era of climate change, pandemics, increasing inequality and
authoritarianism, the idea of installing an authoritarian socialist government
in a revolution becomes more and more acceptable in the minds of many. After
all, if the current capitalist system results in so many people suffering and
dying even in the wealthiest countries of the world, often in cases that
could've been resolved with a more equitable distribution of wealth, then surely
this would justify a violent revolution!

As much as I am not a fan of authoritarian socialism, I might actually agree on
this. If a revolution kills a million people, while preventing tens of
millions from dying, from a pure statistical point of view, that would be a good
outcome. (Granted, I would still oppose an authoritarian socialist government on
the basis of it being authoritarian, but that's beside the point.) But I believe
that we also need a way to actually determine whether that is the case.

Here I've tried coming up with a formula that tries to be as accurate as
possible and calculate how many "deaths of regime" and "deaths of revolution"
would occur.

---

This formula should be applied to the time period of between (10 years before
the revolution) and (10 years after the end of the revolution). It
specifically counts deaths caused by and in whichever country the revolution
would happen.

* **avoidable deaths**: deaths caused due to inadequate distribution of
  resources or unnecessary restrictions. starvation, homelessness, preventable
  disease, etc. -- all contribute towards this factor.

* **direct deaths**: deaths caused specifically by murder or by
  negligence/indifference to a person whose life is in another person/org's
  hands -- from police brutality and otherwise avoidable prisoner deaths to
  all the people being killed during the revolution or executed after it.

Direct deaths are all counted against whichever side committed them.

* Before the revolution, all avoidable deaths count against the
government (obviously).

* When the revolution is ongoing, direct deaths are attributed to whichever side
caused them. Avoidable deaths count towards the government up until they hit
the pre-revolution average for the time period. After that, the remainder counts
for the revolution instead.

> It would be unfair to count every death against the revolution, since their
> government is yet to be implemented, but it's also clear that a violent
> revolution would also make it harder for the government to function and can
> make starvation or homelessness even more widespread. (But it is also possible
> that the revolutionaries also establish institutions that provide food,
> shelter and other resources to people in need.) (Also, if it turns out that
> the government intentionally withheld food or resources from certain people,
> their deaths would always count against the government.)

* After the revolution is finished, avoidable deaths all count against the
revolution instead. It's the revolutionary government's job to recover from the
war and to rebuild the society in a way that eliminates avoidable deaths as much
as possible.

---

I can very well imagine that this set of rules ends up being somewhat biased in
one way or another. If you have any objections or suggestions, my contact info
is on the [about page](../about_me.html). I don't think all revolution is bad,
or that an authoritarian government is incapable of doing good things. But what
I want the most is for any revolution that tries to hold capitalism accountable
for its atrocities, inefficiencies and atrocious inefficiencies to also hold
_itself_ accountable for things that happen under its watch.
