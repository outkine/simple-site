% midnight commander tips
% /dev/urandom
% 2021-03-17

Assorted key combinations that are useful in MC.

<!-- cut -->

Copy all the selected file names into the command line: `Ctrl+X, T`

Make a symbolic link, using absolute paths: `Ctrl+X, S`

Make a symbolic link, using relative paths: `Ctrl+X, V`
