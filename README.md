# rnd's website source code

This is the source code to rnd's website at <https://cutiehaven.club/>.

Included are all the files and scripts used to build it, with the exception of
some files containing other identifying information and copyrighted contents
that might break gitlab's terms of service.
