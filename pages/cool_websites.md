% cool websites
% /dev/urandom
% january 2021

<style>
body {
  background: #221f31;
}
.content {
  color: #fefefe;
  padding: 40px;
}
.linkbuttons img {
  width: 88px;
  height: 31px;
}
</style>

# list of cool websites

This is a list of websites, both on Neocities and elsewhere, that I consider
cool, and that I'd like to promote.

(If you're reading this on a remotely modern web browser, buttons with
additional tooltips will be marked with a thicker green border.)

> %linkbuttons%
> [![Neocities](/banners/neocities.gif "Neocities, a free homepage hosting service")](https://neocities.org/)
> [![Bytemoth's Brook](https://bytemoth.neocities.org/88s/i-brook.png)](https://bytemoth.neocities.org/)
> [![exo.pet](https://exo.pet/images/buttons/88x31/exopetbutton2.png)](https://exo.pet/)
> [![The Lychee Zone](/banners/lychee_zone.gif)](https://averylychee.neocities.org)
> [![InvisibleUp](https://invisibleup.com/static/links/invis.gif)](https://invisibleup.com/)
> [![Rusty the Retro Panda](/banners/rusty.gif)](https://rusty.rustedlogic.net/)
> [![noclip.website](/banners/noclip.gif "noclip.website, featuring 3d viewers for lots of video game levels" )](https://noclip.website/)
> [![Disc Content](https://disc-content.neocities.org/images/disc-content-button.gif "Disc Content, a blog about PS1 games")](https://disc-content.neocities.org/)
> [![marijn.uk](https://marijn.uk/buttons/marijn.uk.png)](https://marijn.uk/)
> [![Alpine Linux](/banners/alpine.gif "Alpine Linux, a lightweight and secure Linux distribution based on musl libc")](https://alpinelinux.org)
> [![The Dreamcast Junkyard](/banners/dcjy.gif "The Dreamcast Junkyard, a blog about all things Sega Dreamcast")](https://thedreamcastjunkyard.co.uk/)
> [![dreamcast live](/banners/dclive.gif "Dreamcast Live, a website all about modern-day ways to play online with the Dreamcast")](https://dreamcastlive.net)
> [![BLASEBALL](/banners/blaseball.gif)](https://blaseball.com)
> [![F-Droid](/banners/fdroid.gif "F-Droid, a repository of free and open-source software for Android phones.")](https://f-droid.org)
> [![Jet Set Radio Live](/banners/jsrl.gif)](https://jetsetradio.live)
> [![Ephinea PSO:BB server](/banners/ephinea.gif)](https://ephinea.pioneer2.net/)
> [![Bomb Rush Cyberfunk](/banners/brc.gif)](https://team-reptile.com/bomb-rush-cyberfunk/)
> [![Bad Game Hall of Fame](/banners/bghof.gif)](https://www.badgamehalloffame.com/)
> [![seximal.net](/banners/seximal.gif "jan Misali's website about seximal
-- the base-6 numbering system")](https://seximal.net)
