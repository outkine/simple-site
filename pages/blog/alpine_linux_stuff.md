% Alpine Linux
% /dev/urandom
% 2021-01-15

This page will host an assortment of tips and tricks collected while using
Alpine Linux.

<!-- cut -->

# About Alpine Linux

[Alpine Linux](https://alpinelinux.org) is a minimalist Linux distro built with
musl libc (as opposed to glibc), OpenRC as an init system and busybox as the
default set of shell utilities. It is frequently used in Docker containers due
to its small size, but it can also make a good workstation distribution, if you
are willing to make some sacrifices. (In fact, I use it to run my personal work
computers.)

Its package manager, `apk`, is fast and easy to use, and creating one's own
packages is performed by building from an `APKBUILD` file very similar to Arch
Linux's `PKGBUILD`s.

This page will include various information that I found useful regarding this
OS.

## apk

`apk` is Alpine Linux's package manager. Its interface is similar to that of
`apt` or `yum`, where commands are given as words (in contrast to `pacman`,
where commands are single-letter arguments).

In addition, there's `abuild`, a system for building packages that's quite
similar to the one in Arch Linux -- in fact, `APKBUILD` files that define how a
package is built are in many ways just `PKGBUILD` files with slightly different
syntax.

An interesting feature is that `apk` always keeps track of, and automatically
removes, any orphaned packages (packages that were installed to satisfy a
dependency of another package, but are no longer necessary because that package
was removed or no longer depends on it).

### list apk package sizes

> %error%
> This script has been broken by a recent update, which displays all package
> size information in a "human-readable" format as opposed to a number of bytes.
> As of the moment of this update, there's no way to disable that change.

This script will list the sizes of all packages in your Alpine Linux install.

    #!/bin/bash
      
    PACKAGES=`apk info -e -s \*`
    
    a=`echo "$PACKAGES" | awk 'NR % 3 == 1' | cut -d ' ' -f 1`
    PKGNAMES=($a)
    a=`echo "$PACKAGES" | awk 'NR % 3 == 2'`
    PKGSIZES=($a)
    
    for i in ${!PKGNAMES[@]}; do
            echo -e "${PKGSIZES[$i]} - ${PKGNAMES[$i]}"
    done

Run it with `./apksize.sh | sort -n | uniq` to get a properly-sorted and
deduplicated list.

## vim shell

By default, the shell is `/bin/ash`, which is a hardlink to busybox. Vim's
default shellpipe options don't account for that, which results in `:make` and
other similar commands missing any text outputted to stderr. Use

    set shell=/bin/sh

in your .vimrc to set the shell to `/bin/sh` and fix that.

## russian console keymap

The default console keymaps that Alpine Linux offers to set up don't provide any way to
switch between keyboard layouts. If you're a Russian user, this means that you
shouldn't use the Russian keymap, otherwise it will be impossible to work in the
console. 

But there is a way to fix that. First, you need to install the `kbd` and
`kbd-legacy` packages. The former contains the tools needed to set up keymaps,
and the latter contains a bunch of unique keymaps.

Then, edit the `/etc/conf.d/loadkeys` file to contain the following line:

```
keymap="legacy/i386/qwerty/ruwin_cplk-UTF-8"
```

This is a special layout that contains English and Russian keys, switchable by
the `Caps Lock` key. There are also variants with `alt`, `alt_sh`, `ct_sh` and
`ctrl` in the name, presumably to indicate that in these, the layout is switched
by the `Alt`, `Alt+Shift`, `Ctrl+Shift` and `Ctrl` keys and combinations
respectively, but I haven't tried those, so I can't say if they work.
